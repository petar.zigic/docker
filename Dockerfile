FROM --platform=linux/amd64 alpine

RUN apk update
RUN apk add curl
RUN apk add busybox-extras
RUN apk add bind-tools
RUN apk add iptables
RUN apk add nmap
RUN apk add ipvsadm
